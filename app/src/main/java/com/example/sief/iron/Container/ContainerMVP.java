package com.example.sief.iron.Container;

/**
 * Created by sief on 3/14/2018.
 */

public interface ContainerMVP {
    interface HomeView {
        void goToFirstSectionsActivity(int position);

        void goToAnglesActivity(int position);

        void goToISectionsActivity(int position);

        void goToUSectionsActivity(int position);

        void goToUPSectionsActivity(int position);

        void goToTubeSectionsActivity(int position);

        void goToTubeRoundActivity(int position);

        void goToFlatsActivity(int position);
    }

    interface HomePresenter {
        void decideDestination(int position);
    }
}
