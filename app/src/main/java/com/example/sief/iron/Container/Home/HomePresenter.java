package com.example.sief.iron.Container.Home;

/**
 * Created by sief on 3/19/2018.
 */

public class HomePresenter implements HomeMVP.presenter {
    HomeMVP.firstView firstView;
    HomeMVP.anglesView anglesView;
    HomeMVP.iView iView;
    HomeMVP.tubeRoundView tubeRoundView;
    HomeMVP.upView upView;
    HomeMVP.tubeView tubeView;
    HomeMVP.uView uView;
    HomeMVP.flatsView flatsView;

    public HomePresenter(HomeMVP.firstView firstView) {

        this.firstView = firstView;
    }

    public HomePresenter(HomeMVP.anglesView anglesView) {

        this.anglesView = anglesView;
    }

    public HomePresenter(HomeMVP.uView uView) {

        this.uView = uView;
    }

    public HomePresenter(HomeMVP.tubeView tubeView) {

        this.tubeView = tubeView;
    }

    public HomePresenter(HomeMVP.upView upView) {

        this.upView = upView;
    }

    public HomePresenter(HomeMVP.tubeRoundView tubeRoundView) {

        this.tubeRoundView = tubeRoundView;
    }

    public HomePresenter(HomeMVP.iView iView) {

        this.iView = iView;
    }

    public HomePresenter(HomeMVP.flatsView flatsView) {

        this.flatsView = flatsView;
    }

    @Override
    public void initializeView(int position) {
        switch (position) {
            case 0:
                firstView.initializeBarsCircle();
                break;
            case 1:
                firstView.initializeBarsSquare();
                break;
            case 2:
                firstView.initializeBarsHexagon();
                break;
            case 3:
                firstView.initializeSheets();
                break;
            case 4:
                flatsView.initializeFlatsView();
                break;
            case 5:
                anglesView.initializeAngleE20();
                break;
            case 6:
                anglesView.initializeAngleE70();
                break;
            case 7:
                anglesView.initializeAngleU30();
                break;
            case 8:
                anglesView.initializeAngleU90();
                break;
            case 9:
                uView.initializeUChannel();
                break;
            case 10:
                iView.initializeBeamsIPE();
                break;
            case 11:
                iView.initializeBeamsIPEAA();
                break;
            case 12:
                iView.initializeHEA();
                break;
            case 13:
                iView.initializeHEB();
                break;
            case 14:
                iView.initializeJIS();
                break;
            case 15:
                upView.initializeUPE();
                break;
            case 16:
                upView.initializeUPN();
                break;
            case 17:
                tubeView.initializeTubeSquare();
                break;
            case 18:
                tubeView.initializeTubeRectangular();
                break;
            case 19:
                tubeRoundView.initializeTubeRound();
                break;
            default:
                break;
        }
    }
}
