package com.example.sief.iron.Container.Home.FirstSections;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sief.iron.Container.Home.HomeMVP;
import com.example.sief.iron.Container.Home.HomePresenter;
import com.example.sief.iron.R;
import com.example.sief.iron.Utils.MySingleton;
import com.warkiz.widget.IndicatorSeekBar;

public class FirstSectionsActivity extends AppCompatActivity implements HomeMVP.firstView {

    private HomeMVP.presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_sections);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new HomePresenter(this);
        presenter.initializeView(getIntent().getExtras().getInt("position"));
    }

    /*
     *  Customizing the view to Bars-circle view
     */
    @Override
    public void initializeBarsCircle() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[0]);
        final TextView calcOne, calcTwo, calcThree, calcFour, calcFive, calcSix, calcSeven, calcEight;
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.bar_circle);
        ((LinearLayout) findViewById(R.id.container_three)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.container_four)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.container_five)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.container_six)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.container_seven)).setVisibility(View.VISIBLE);
        ((LinearLayout) findViewById(R.id.container_eight)).setVisibility(View.VISIBLE);

        ((View) findViewById(R.id.separator)).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.title_one)).setText(getString(R.string.f_5_t_50));
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.weight));
        ((TextView) findViewById(R.id.title_three)).setText(getString(R.string.section));
        ((TextView) findViewById(R.id.title_four)).setText(getString(R.string.circumference));
        ((TextView) findViewById(R.id.title_five)).setText(getString(R.string.f_52_t_220));
        ((TextView) findViewById(R.id.title_six)).setText(getString(R.string.weight));
        ((TextView) findViewById(R.id.title_seven)).setText(getString(R.string.section));
        ((TextView) findViewById(R.id.title_eight)).setText(getString(R.string.circumference));

        ((TextView) findViewById(R.id.units_two)).setText(getString(R.string.kg_m));
        ((TextView) findViewById(R.id.units_three)).setText(getString(R.string.cm_sq));
        ((TextView) findViewById(R.id.units_four)).setText(getString(R.string.mm));
        ((TextView) findViewById(R.id.units_six)).setText(getString(R.string.kg_m));
        ((TextView) findViewById(R.id.units_seven)).setText(getString(R.string.cm_sq));
        ((TextView) findViewById(R.id.units_eight)).setText(getString(R.string.mm));

        ((TextView) findViewById(R.id.bar_circle_label)).setText(R.string.dimensions_in_millimeters);

        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        calcFour = (TextView) findViewById(R.id.calculation_four);
        if (detectScreenSize() > 600) {
            ((IndicatorSeekBar) findViewById(R.id.indicator_seek_bar_small)).setVisibility(View.GONE);
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                    calcThree.setText(((progress * 2) * 2) + "");
                    calcFour.setText((((progress * 2) * 2) * 2) + "");
                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        } else {
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                    calcThree.setText(((progress * 2) * 2) + "");
                    calcFour.setText((((progress * 2) * 2) * 2) + "");
                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }
    }

    /*
     *  Customizing the view to Bars-Square view
     */
    @Override
    public void initializeBarsSquare() {
        final TextView calcOne, calcTwo;
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[1]);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.bar_square);
        ((TextView) findViewById(R.id.title_one)).setText(getString(R.string.d));
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.weight));
        ((TextView) findViewById(R.id.units_two)).setText(getString(R.string.kg_m));
        ((TextView) findViewById(R.id.bar_circle_label)).setVisibility(View.GONE);

        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        if (detectScreenSize() > 600) {
            ((IndicatorSeekBar) findViewById(R.id.indicator_seek_bar)).setVisibility(View.GONE);
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar_small);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        } else {
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

    }

    /*
     *  Customizing the view to Bars-Hexagon view
     */
    @Override
    public void initializeBarsHexagon() {
        final TextView calcOne, calcTwo;
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[2]);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.bar_hexagon);
        ((TextView) findViewById(R.id.title_one)).setText(getString(R.string.d));
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.weight));
        ((TextView) findViewById(R.id.units_two)).setText(getString(R.string.kg_m));

        ((TextView) findViewById(R.id.bar_circle_label)).setVisibility(View.GONE);

        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);

        if (detectScreenSize() > 600) {
            ((IndicatorSeekBar) findViewById(R.id.indicator_seek_bar)).setVisibility(View.GONE);
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar_small);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        } else {
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

    }


    /*
     *  Customizing the view to Sheets view
     */
    @Override
    public void initializeSheets() {
        final TextView calcOne, calcTwo, textView;
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[3]);
        ((LinearLayout) findViewById(R.id.container)).setOrientation(LinearLayout.VERTICAL);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.sheets);
        ((TextView) findViewById(R.id.title_one)).setText("");
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.weight));
        ((TextView) findViewById(R.id.units_two)).setText(getString(R.string.kg_m));
        TextView dimensionTitle = (TextView) findViewById(R.id.bar_circle_label);
        dimensionTitle.setText(R.string.dimensions_in_millimeters_sheet);
        dimensionTitle.setPaddingRelative(24, 0, 0, 32);


        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        textView = (TextView) findViewById(R.id.txt);
        textView.setVisibility(View.VISIBLE);
        textView.setPaddingRelative(0, 0, 0, 32);
        if (detectScreenSize() > 600) {
            ((IndicatorSeekBar) findViewById(R.id.indicator_seek_bar)).setVisibility(View.GONE);
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar_small);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                    textView.setText("weight is " + (progress * 2));

                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        } else {
            IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
            indicatorSeekBar.setMax(50);
            indicatorSeekBar.setMin(1);
            indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                    calcOne.setText(progress + "");
                    calcTwo.setText((progress * 2) + "");
                    textView.setText("weight is " + (progress * 2));

                }

                @Override
                public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

                }

                @Override
                public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

                }

                @Override
                public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

                }
            });
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

    }

    /*
     *  Detect the screen size of the device in dp
     */
    private float detectScreenSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        float widthDp = width / scaleFactor;
        return widthDp;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
