package com.example.sief.iron.Container.Home.TubeSections;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sief.iron.Container.Home.HomeMVP;
import com.example.sief.iron.Container.Home.HomePresenter;
import com.example.sief.iron.R;
import com.example.sief.iron.Utils.MySingleton;
import com.warkiz.widget.IndicatorSeekBar;

public class TubeSectionsActivity extends AppCompatActivity implements HomeMVP.tubeView {

    HomeMVP.presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tube_sections);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new HomePresenter(this);
        presenter.initializeView(getIntent().getExtras().getInt("position"));
    }

    @Override
    public void initializeTubeSquare() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[17]);
        final TextView calcOne, calcTwo, calcThree, textOne, textTwo;
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                calcThree.setText((progress * 2) + "");
                textOne.setText((progress * 2) + "");
                textTwo.setText((progress * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeTubeRectangular() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[18]);
        final TextView calcOne, calcTwo, calcThree, textOne, textTwo;
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.tube_rectangular);
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                calcThree.setText((progress * 2) + "");
                textOne.setText((progress * 2) + "");
                textTwo.setText((progress * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
