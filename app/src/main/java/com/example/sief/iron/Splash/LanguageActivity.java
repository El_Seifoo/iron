package com.example.sief.iron.Splash;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioGroup;

import com.example.sief.iron.Container.ContainerActivity;
import com.example.sief.iron.R;
import com.example.sief.iron.Utils.MySingleton;

import java.util.Locale;

public class LanguageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MySingleton.getmInstance(LanguageActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            Locale localeAr = new Locale("ar", "EG");
            setLocale(localeAr, getString(R.string.settings_language_arabic_value));
        } else if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_english_value))) {
            Locale localeEn = new Locale("en");
            setLocale(localeEn, getString(R.string.settings_language_english_value));
        }
        setContentView(R.layout.activity_language);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.language_group_container);
        radioGroup.getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MySingleton.getmInstance(LanguageActivity.this).getAppLang().equals(getString(R.string.settings_language_english_value))) {
                    MySingleton.getmInstance(LanguageActivity.this).setAppLang(getString(R.string.settings_language_english_value));
                }
                goContainerActivity();
            }
        });

        radioGroup.getChildAt(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!MySingleton.getmInstance(LanguageActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    MySingleton.getmInstance(LanguageActivity.this).setAppLang(getString(R.string.settings_language_arabic_value));
                }
                goContainerActivity();
            }
        });
    }

    private void setLocale(Locale locale, String lang) {
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getApplicationContext().getResources().updateConfiguration(configuration, null);
//        Resources resources = getResources();
//        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
//        Configuration configuration = resources.getConfiguration();
//        configuration.locale = locale;
        if (lang.equals(getString(R.string.settings_language_arabic_value))) {
            configuration.setLayoutDirection(new Locale("ar", "EG"));
        } else if (lang.equals(getString(R.string.settings_language_english_value))) {
            configuration.setLayoutDirection(new Locale("en"));
        }
//        resources.updateConfiguration(configuration, displayMetrics);
    }

    private void goContainerActivity() {
        Intent intent = new Intent(LanguageActivity.this, ContainerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        MySingleton.getmInstance(LanguageActivity.this).firstTime();
    }
}
