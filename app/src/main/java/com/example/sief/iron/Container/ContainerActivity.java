package com.example.sief.iron.Container;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.sief.iron.Container.Ads.AdsActivity;
import com.example.sief.iron.Container.Contact.ContactActivity;
import com.example.sief.iron.Container.AboutApp.AboutAppActivity;
import com.example.sief.iron.Container.AboutUs.AboutUsActivity;
import com.example.sief.iron.Container.Home.HomeFragment;
import com.example.sief.iron.Container.Profile.ProfileActivity;
import com.example.sief.iron.R;
import com.example.sief.iron.Utils.MySingleton;

import java.util.Locale;

public class ContainerActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private Toolbar toolbar;
    // index to know the current nav menu item
    public static int navItemIndex = 0;
    // Tag for attaching the fragments
    private static final String TAG_HOME = "home";

    // Tag for current attaching fragment
    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles based on selected nav menu item
    private String[] activityTitles;
    // flag to load makeOrder fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            Locale localeAr = new Locale("ar", "EG");
            setLocale(localeAr, getString(R.string.settings_language_arabic_value));
        } else if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_english_value))) {
            Locale localeEn = new Locale("en");
            setLocale(localeEn, getString(R.string.settings_language_english_value));
        }
        setContentView(R.layout.activity_container);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        // get toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        // load navigation header
        loadHeader();
        // initialize navigation
        setUpNavigation();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment();
        }
    }

    /*
     *  Change the configuration of the Application depending on App Language
     */
    private void setLocale(Locale locale, String lang) {
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getApplicationContext().getResources().updateConfiguration(configuration, null);
//        Resources resources = getResources();
//        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
//        Configuration configuration = resources.getConfiguration();
//        configuration.locale = locale;
        if (lang.equals(getString(R.string.settings_language_arabic_value))) {
            configuration.setLayoutDirection(new Locale("ar", "EG"));
        } else if (lang.equals(getString(R.string.settings_language_english_value))) {
            configuration.setLayoutDirection(new Locale("en"));
        }
//        resources.updateConfiguration(configuration, displayMetrics);
    }


    /*
     *  Load Navigation view header data
     */
    public void loadHeader() {

    }

    /*
     *  Initialize navigation ...
     *
     */
    public void setUpNavigation() {
        // Handle navigation menu items actions
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.nav_profile:
                        // start Profile activity
                        startActivity(new Intent(ContainerActivity.this, ProfileActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_ads:
                        // start ads activity
                        startActivity(new Intent(ContainerActivity.this, AdsActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_about_us:
                        // start About us activity
                        startActivity(new Intent(ContainerActivity.this, AboutUsActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_about_app:
                        // start About app activity
                        startActivity(new Intent(ContainerActivity.this, AboutAppActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_contact:
                        // start Contact activity
                        startActivity(new Intent(ContainerActivity.this, ContactActivity.class));
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_login:
                        // start Login
                        return true;
                    case R.id.nav_logout:
                        // Logout
                        Intent intent = getIntent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        drawer.closeDrawers();
                        break;
                    case R.id.nav_language:
                        // Language
                        if (MySingleton.getmInstance(ContainerActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            MySingleton.getmInstance(ContainerActivity.this).setAppLang(getString(R.string.settings_language_english_value));
                        } else if (MySingleton.getmInstance(ContainerActivity.this).getAppLang().equals(getString(R.string.settings_language_english_value))) {
                            MySingleton.getmInstance(ContainerActivity.this).setAppLang(getString(R.string.settings_language_arabic_value));
                        }
                        finish();
                        startActivity(getIntent());
                        drawer.closeDrawers();
                        return true;
                    default:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                }

                loadHomeFragment();

                return true;
            }
        });

        android.support.v7.app.ActionBarDrawerToggle actionBarDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                FrameLayout frame = (FrameLayout) findViewById(R.id.frame_container);
                if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
                    frame.setX(-slideOffset * drawerView.getWidth());
                } else {
                    frame.setX(slideOffset * drawerView.getWidth());
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    /*
     *  return selected fragment from navigation menu
     */
    public void loadHomeFragment() {

        // selecting appropriate navigation menu item
        selectNavmenu();

        // set toolbar title
        setToolbarTitle();

        // close the navigation drawer if the user selected the current navigation menu item
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getSelectedFragment();
                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    fragmentTransaction.replace(R.id.frame, fragment);
                    fragmentTransaction.commitAllowingStateLoss();
                }
            }
        };

        if (runnable != null) {
            mHandler.post(runnable);
        }

        // closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    public void selectNavmenu() {
        for (int i = 0; i < 10; i++) {
            if (i == navItemIndex) {
                navigationView.getMenu().getItem(i).setChecked(true);
            } else {
                navigationView.getMenu().getItem(i).setChecked(false);
            }
        }
    }

    public void setToolbarTitle() {

        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    /*
     *  Return selected fragment
     */
    public Fragment getSelectedFragment() {
        switch (navItemIndex) {
            case 0:
                return HomeFragment.newInstance();
            default:
                return HomeFragment.newInstance();
        }
    }

    /*
     *  Handle social media links
     */
    public void socialMedia(View view) {
        switch (view.getId()) {
            case R.id.twitter:
                Toast.makeText(this, "Twitter", Toast.LENGTH_SHORT).show();
                break;
            case R.id.facebook:
                Toast.makeText(this, "Facebook", Toast.LENGTH_SHORT).show();
                break;
            case R.id.instagram:
                Toast.makeText(this, "Instagram", Toast.LENGTH_SHORT).show();
                break;
            case R.id.youtube:
                Toast.makeText(this, "Youtube", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // load HomeFragment if user pressed back in other fragment
        if (shouldLoadHomeFragOnBackPress) {
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_HOME;
                loadHomeFragment();
                return;
            }
        }
        super.onBackPressed();
    }
}
