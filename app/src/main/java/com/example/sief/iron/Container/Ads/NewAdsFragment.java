package com.example.sief.iron.Container.Ads;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sief.iron.Container.Home.HomeFragment;
import com.example.sief.iron.R;

/**
 * Created by sief on 3/16/2018.
 */

public class NewAdsFragment extends Fragment {
    /*
     *  Static method to initialize fragment
     *  no params
     *  return NewAdsFragment
     */
    public static NewAdsFragment newInstance() {
        return new NewAdsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_ads, container, false);
        return view;
    }
}
