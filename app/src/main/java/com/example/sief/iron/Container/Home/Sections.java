package com.example.sief.iron.Container.Home;

/**
 * Created by sief on 3/18/2018.
 */

public class Sections {
    private String title;
    private int pic;

    public Sections() {
    }

    public Sections(String title, int pic) {
        this.title = title;
        this.pic = pic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPic() {
        return pic;
    }

    public void setPic(int pic) {
        this.pic = pic;
    }
}
