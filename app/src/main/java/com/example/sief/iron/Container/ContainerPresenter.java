package com.example.sief.iron.Container;

/**
 * Created by sief on 3/14/2018.
 */

public class ContainerPresenter implements ContainerMVP.HomePresenter {
    ContainerMVP.HomeView homeView;

    public ContainerPresenter(ContainerMVP.HomeView homeView) {
        this.homeView = homeView;
    }


    /*
     *  Check the position of each item
     *  to decide which activity should
     *  navigate to
     */
    @Override
    public void decideDestination(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
                homeView.goToFirstSectionsActivity(position);
                break;
            case 4:
                homeView.goToFlatsActivity(position);
                break;
            case 5:
            case 6:
            case 7:
            case 8:
                homeView.goToAnglesActivity(position);
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                homeView.goToISectionsActivity(position);
                break;
            case 9:
                homeView.goToUSectionsActivity(position);
                break;
            case 15:
            case 16:
                homeView.goToUPSectionsActivity(position);
                break;
            case 17:
            case 18:
                homeView.goToTubeSectionsActivity(position);
                break;
            case 19:
                homeView.goToTubeRoundActivity(position);
                break;
            default:
                break;
        }
    }
}
