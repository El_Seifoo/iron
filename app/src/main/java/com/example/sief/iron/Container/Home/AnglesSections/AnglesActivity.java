package com.example.sief.iron.Container.Home.AnglesSections;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sief.iron.Container.Home.HomeMVP;
import com.example.sief.iron.Container.Home.HomePresenter;
import com.example.sief.iron.R;
import com.example.sief.iron.Utils.MySingleton;
import com.warkiz.widget.IndicatorSeekBar;

public class AnglesActivity extends AppCompatActivity implements HomeMVP.anglesView {

    HomeMVP.presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angles);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new HomePresenter(this);
        presenter.initializeView(getIntent().getExtras().getInt("position"));
    }

    @Override
    public void initializeAngleE20() {


        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[5]);
        final TextView calcOne, calcTwo, textOne, textTwo;
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setMax(50);
        indicatorSeekBar.setMin(1);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeAngleE70() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[6]);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.angle_equal_70);
        ((TextView) findViewById(R.id.section_title)).setText(getString(R.string.angle_e_70));
        final TextView calcOne, calcTwo, textOne, textTwo;
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setMax(50);
        indicatorSeekBar.setMin(1);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeAngleU30() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[7]);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.angle_unequal_30);
        ((TextView) findViewById(R.id.section_title)).setText(getString(R.string.angle_u_30));
        final TextView calcOne, calcTwo, textOne, textTwo, textThree;
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three);
        textThree.setVisibility(View.VISIBLE);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setMax(50);
        indicatorSeekBar.setMin(1);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText(((((progress * 2) * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeAngleU90() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[8]);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.angle_unequal_90);
        ((TextView) findViewById(R.id.section_title)).setText(getString(R.string.angle_u_90));
        final TextView calcOne, calcTwo, textOne, textTwo, textThree;
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three);
        textThree.setVisibility(View.VISIBLE);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setMax(50);
        indicatorSeekBar.setMin(1);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText(((((progress * 2) * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_filter) {
            Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.filter_dialog);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sections_menu, menu);
        return true;
    }


}
