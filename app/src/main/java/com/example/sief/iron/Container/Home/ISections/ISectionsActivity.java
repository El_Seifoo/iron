package com.example.sief.iron.Container.Home.ISections;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sief.iron.Container.Home.HomeMVP;
import com.example.sief.iron.Container.Home.HomePresenter;
import com.example.sief.iron.R;
import com.example.sief.iron.Utils.MySingleton;
import com.warkiz.widget.IndicatorSeekBar;

public class ISectionsActivity extends AppCompatActivity implements HomeMVP.iView {
    HomeMVP.presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isections);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new HomePresenter(this);
        presenter.initializeView(getIntent().getExtras().getInt("position"));
    }

    @Override
    public void initializeBeamsIPE() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[10]);
        final TextView calcOne, calcTwo, calcThree, calcFour, textOne, textTwo, textThree, textFour;
        calcOne = (TextView) findViewById(R.id.calculation_one);
        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        calcFour = (TextView) findViewById(R.id.calculation_four);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three_beam);
        textFour = (TextView) findViewById(R.id.text_four);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcOne.setText(progress + "");
                calcTwo.setText((progress * 2) + "");
                calcThree.setText((progress * 2) + "");
                calcFour.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText((((progress * 2) * 2) * 2) + "");
                textFour.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeBeamsIPEAA() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[11]);
        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.beam_ipeaa);
        final TextView calcThree, calcFour, textOne, textTwo, textThree, textFour;
        ((LinearLayout) findViewById(R.id.container_one)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.container_two)).setVisibility(View.GONE);

        calcThree = (TextView) findViewById(R.id.calculation_three);
        calcFour = (TextView) findViewById(R.id.calculation_four);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three_beam);
        textFour = (TextView) findViewById(R.id.text_four);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcThree.setText((progress * 2) + "");
                calcFour.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText((((progress * 2) * 2) * 2) + "");
                textFour.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeHEA() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[12]);
        final TextView calcTwo, calcThree, calcFour, textOne, textTwo, textThree, textFour;

        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.hea);
        ((LinearLayout) findViewById(R.id.container_one)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.hea));
        ((TextView) findViewById(R.id.units_two)).setText(getString(R.string.economical));
        ((TextView) findViewById(R.id.text_three_beam)).setVisibility(View.GONE);

        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        calcFour = (TextView) findViewById(R.id.calculation_four);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three);
        textThree.setVisibility(View.VISIBLE);
        textFour = (TextView) findViewById(R.id.text_four);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcTwo.setText((progress * 2) + "");
                calcThree.setText((progress * 2) + "");
                calcFour.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText((((progress * 2) * 2) * 2) + "");
                textFour.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeHEB() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[13]);
        final TextView calcTwo, calcThree, calcFour, textOne, textTwo, textThree, textFour;

        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.heb);
        ((LinearLayout) findViewById(R.id.container_one)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.heb));
        ((TextView) findViewById(R.id.units_two)).setText(getString(R.string.economical));
        ((TextView) findViewById(R.id.text_three_beam)).setVisibility(View.GONE);

        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        calcFour = (TextView) findViewById(R.id.calculation_four);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three);
        textThree.setVisibility(View.VISIBLE);
        textFour = (TextView) findViewById(R.id.text_four);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcTwo.setText((progress * 2) + "");
                calcThree.setText((progress * 2) + "");
                calcFour.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText((((progress * 2) * 2) * 2) + "");
                textFour.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void initializeJIS() {
        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[14]);
        final TextView calcTwo, calcThree, calcFour, textOne, textTwo, textThree, textFour;

        ((ImageView) findViewById(R.id.section_img)).setImageResource(R.mipmap.jis);
        ((LinearLayout) findViewById(R.id.container_one)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.title_two)).setText(getString(R.string.jis));
        ((TextView) findViewById(R.id.units_two)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.text_three_beam)).setVisibility(View.GONE);

        calcTwo = (TextView) findViewById(R.id.calculation_two);
        calcThree = (TextView) findViewById(R.id.calculation_three);
        calcFour = (TextView) findViewById(R.id.calculation_four);
        textOne = (TextView) findViewById(R.id.text_one);
        textTwo = (TextView) findViewById(R.id.text_two);
        textThree = (TextView) findViewById(R.id.text_three);
        textThree.setVisibility(View.VISIBLE);
        textFour = (TextView) findViewById(R.id.text_four);
        IndicatorSeekBar indicatorSeekBar = (IndicatorSeekBar) findViewById(R.id.indicator_seek_bar);
        indicatorSeekBar.setOnSeekChangeListener(new IndicatorSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(IndicatorSeekBar seekBar, int progress, float progressFloat, boolean fromUserTouch) {
                calcTwo.setText((progress * 2) + "");
                calcThree.setText((progress * 2) + "");
                calcFour.setText((progress * 2) + "");
                textOne.setText(((progress * 2) * 2) + "");
                textTwo.setText((((progress * 2) * 2) * 2) + "");
                textThree.setText((((progress * 2) * 2) * 2) + "");
                textFour.setText((((progress * 2) * 2) * 2) + "");
            }

            @Override
            public void onSectionChanged(IndicatorSeekBar seekBar, int thumbPosOnTick, String textBelowTick, boolean fromUserTouch) {

            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar, int thumbPosOnTick) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            ((RelativeLayout) findViewById(R.id.section_img_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            indicatorSeekBar.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
