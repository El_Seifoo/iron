package com.example.sief.iron.Container.Home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.sief.iron.Container.ContainerMVP;
import com.example.sief.iron.Container.ContainerPresenter;
import com.example.sief.iron.Container.Home.AnglesSections.AnglesActivity;
import com.example.sief.iron.Container.Home.FirstSections.FirstSectionsActivity;
import com.example.sief.iron.Container.Home.FlatsSection.FlatsActivity;
import com.example.sief.iron.Container.Home.ISections.ISectionsActivity;
import com.example.sief.iron.Container.Home.TubeRoundSections.TubeRoundActivity;
import com.example.sief.iron.Container.Home.TubeSections.TubeSectionsActivity;
import com.example.sief.iron.Container.Home.UPSections.UPSectionsActivity;
import com.example.sief.iron.Container.Home.USections.USectionsActivity;
import com.example.sief.iron.R;

import java.util.ArrayList;

/**
 * Created by sief on 3/14/2018.
 */

public class HomeFragment extends Fragment implements HomeListAdapter.ListItemClickListener, ContainerMVP.HomeView {

    private ContainerMVP.HomePresenter presenter;


    /*
     *  Static method to initialize fragment
     *  no params
     *  return HomeFragment
     */
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        /*
         *  but intent data to args if newInstace
         *  has params
         */
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        presenter = new ContainerPresenter(this);
        RecyclerView homeRecyclerView = (RecyclerView) view.findViewById(R.id.home_recycler_view);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        float widthDp = width / scaleFactor;
        Log.e("width ", widthDp + "dp");
        GridLayoutManager layoutManager;
        if (widthDp >= 600) {
            layoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);

        } else {
            layoutManager = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        }
        homeRecyclerView.setLayoutManager(layoutManager);
        ArrayList<Sections> sections = new ArrayList<>();
        String[] titles = getResources().getStringArray(R.array.home_list_titles);
        sections.add(new Sections(titles[0], R.mipmap.menu_bar_circle));
        sections.add(new Sections(titles[1], R.mipmap.menu_bar_square));
        sections.add(new Sections(titles[2], R.mipmap.menu_bar_hexagon));
        sections.add(new Sections(titles[3], R.mipmap.menu_sheets));
        sections.add(new Sections(titles[4], R.mipmap.menu_flats));
        sections.add(new Sections(titles[5], R.mipmap.menu_angle_equal_20));
        sections.add(new Sections(titles[6], R.mipmap.menu_angle_equal_70));
        sections.add(new Sections(titles[7], R.mipmap.menu_angle_uunequal_30));
        sections.add(new Sections(titles[8], R.mipmap.menu_angle_unequal_90));
        sections.add(new Sections(titles[9], R.mipmap.menu_u_channel));
        sections.add(new Sections(titles[10], R.mipmap.menu_beams_ipe));
        sections.add(new Sections(titles[11], R.mipmap.menu_beams_ipeaa));
        sections.add(new Sections(titles[12], R.mipmap.menu_hea));
        sections.add(new Sections(titles[13], R.mipmap.menu_heb));
        sections.add(new Sections(titles[14], R.mipmap.menu_jis));
        sections.add(new Sections(titles[15], R.mipmap.menu_upe));
        sections.add(new Sections(titles[16], R.mipmap.menu_upn));
        sections.add(new Sections(titles[17], R.mipmap.menu_tube_square));
        sections.add(new Sections(titles[18], R.mipmap.menu_tube_rectangular));
        sections.add(new Sections(titles[19], R.mipmap.menu_tube_round));
        HomeListAdapter adapter = new HomeListAdapter(this, sections);
        homeRecyclerView.setAdapter(adapter);

        return view;
    }

    /*
     *  ListItem (Sections) onClick method
     */
    @Override
    public void onListItemClick(int position) {
        presenter.decideDestination(position);
    }


    /*
     *  Create an intent to FirstSectionsActivity
     *  sending listItem position using bundle
     */
    @Override
    public void goToFirstSectionsActivity(int position) {
        Intent intent = new Intent(getContext(), FirstSectionsActivity.class);
        intent.putExtra("position", position);
        startActivity(intent);
    }

    /*
     *  Create an intent to FirstSectionsActivity
     *  sending listItem position using bundle
     */
    @Override
    public void goToAnglesActivity(int position) {
        Intent intent2 = new Intent(getContext(), AnglesActivity.class);
        intent2.putExtra("position", position);
        startActivity(intent2);
    }

    /*
         *  Create an intent to FirstSectionsActivity
         *  sending listItem position using bundle
         */
    @Override
    public void goToISectionsActivity(int position) {
        Intent intent3 = new Intent(getContext(), ISectionsActivity.class);
        intent3.putExtra("position", position);
        startActivity(intent3);
    }

    /*
         *  Create an intent to goToUSectionsActivity
         *  sending listItem position using bundle
         */
    @Override
    public void goToUSectionsActivity(int position) {
        Intent intent4 = new Intent(getContext(), USectionsActivity.class);
        intent4.putExtra("position", position);
        startActivity(intent4);
    }

    /*
     *  Create an intent to goToUPSectionsActivity
     *  sending listItem position using bundle
     */
    @Override
    public void goToUPSectionsActivity(int position) {
        Intent intent5 = new Intent(getContext(), UPSectionsActivity.class);
        intent5.putExtra("position", position);
        startActivity(intent5);
    }

    /*
     *  Create an intent to goToTubeSectionsActivity
     *  sending listItem position using bundle
     */
    @Override
    public void goToTubeSectionsActivity(int position) {
        Intent intent6 = new Intent(getContext(), TubeSectionsActivity.class);
        intent6.putExtra("position", position);
        startActivity(intent6);
    }

    /*
     *  Create an intent to goToTubeRoundActivity
     *  sending listItem position using bundle
     */
    @Override
    public void goToTubeRoundActivity(int position) {
        Intent intent7 = new Intent(getContext(), TubeRoundActivity.class);
        intent7.putExtra("position", position);
        startActivity(intent7);
    }

    @Override
    public void goToFlatsActivity(int position) {
        Intent intent8 = new Intent(getContext(), FlatsActivity.class);
        intent8.putExtra("position", position);
        startActivity(intent8);
    }
}
