package com.example.sief.iron.Utils;

/**
 * Created by sief on 3/16/2018.
 */

public class Constants {

    public static final String SHARED_PREF_NAME = "iron";
    public static final String USER_TOKEN = "token";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String APP_LANGUAGE = "appLang";
    public static final String FIRST_TIME = "firstTime";
}
