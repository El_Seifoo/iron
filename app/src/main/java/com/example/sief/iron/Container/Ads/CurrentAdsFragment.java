package com.example.sief.iron.Container.Ads;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sief.iron.R;

/**
 * Created by sief on 3/16/2018.
 */

public class CurrentAdsFragment extends Fragment {
    /*
     *  Static method to initialize fragment
     *  no params
     *  return NewAdsFragment
     */
    public static CurrentAdsFragment newInstance() {
        return new CurrentAdsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_ads, container, false);
        return view;
    }
}
