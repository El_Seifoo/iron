package com.example.sief.iron.Container.Home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.sief.iron.R;

import java.util.ArrayList;

/**
 * Created by sief on 3/16/2018.
 */

public class HomeListAdapter extends RecyclerView.Adapter<HomeListAdapter.Holder> {


    private ArrayList<Sections> sections;
    final private ListItemClickListener mOnClickListener;

    public HomeListAdapter(ListItemClickListener listener, ArrayList<Sections> sections) {
        mOnClickListener = listener;
        this.sections = sections;
        notifyDataSetChanged();
    }

    public interface ListItemClickListener {
        void onListItemClick(int position);
    }

    @NonNull
    @Override
    public HomeListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeListAdapter.Holder holder, int position) {

        holder.sectionImg.setImageResource(sections.get(position).getPic());
        holder.sectionTitle.setText(sections.get(position).getTitle());
    }


    @Override
    public int getItemCount() {
        return (null != sections ? sections.size() : 0);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView sectionTitle;
        ImageView sectionImg;

        public Holder(View itemView) {
            super(itemView);
            sectionTitle = (TextView) itemView.findViewById(R.id.sections_title);
            sectionImg = (ImageView) itemView.findViewById(R.id.sections_img);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mOnClickListener.onListItemClick(position);
        }
    }
}
