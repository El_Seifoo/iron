package com.example.sief.iron.Utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.sief.iron.R;

/**
 * Created by sief on 3/16/2018.
 */

public class MySingleton extends Application {
    private static MySingleton mInstance;
    private RequestQueue requestQueue;
    private static Context context;
    private SharedPreferences sharedPreferences;

    private MySingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
        sharedPreferences = getSharedPreferences();
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static synchronized MySingleton getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }

        return mInstance;
    }


    public void addToRQ(Request request) {
        requestQueue.add(request);
    }


    public SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    /*
     *  Setting application language
     *  take one parameter (language : String)
     *  save language using sharedPref
     */
    public void setAppLang(String language) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(Constants.APP_LANGUAGE, language);
        editor.apply();
    }

    /*
     *  getting language of the application from saved
     *  data in sharedPref
     */
    public String getAppLang() {
        return getSharedPreferences().getString(Constants.APP_LANGUAGE, context.getString(R.string.settings_language_default));
    }


    /*
     *  Save that it's not first time of user
     */
    public void firstTime() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(Constants.FIRST_TIME, false);
        editor.apply();
    }

    /*
     *  Check if it's first time or not !
     */
    public boolean isFirstTime() {
        return getSharedPreferences().getBoolean(Constants.FIRST_TIME, true);
    }

    /*
     *  Saving user token
     *  take one param (token : String)
     *  save token using sharedPref
     */
    public void saveUserToken(String token) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(Constants.USER_TOKEN, token);
        editor.apply();
    }

    /*
     *  Getting userToken
     */
    public String getUserToken() {
        return sharedPreferences.getString(Constants.USER_TOKEN, "");
    }


    /*
     *  saving that user logged in
     */
    public void loginUser() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();
    }

    /*
     *  Check if user is logged in or not
     */
    public boolean isLoggedIn() {
        return getSharedPreferences().getBoolean(Constants.IS_LOGGED_IN, false);
    }

    /*
     *  Logging the user out by deleting all
     *  user data from sharedPref
     */
    public void logout() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear();
        editor.apply();
    }
}
