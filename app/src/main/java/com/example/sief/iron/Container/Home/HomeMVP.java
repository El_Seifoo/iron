package com.example.sief.iron.Container.Home;

/**
 * Created by sief on 3/19/2018.
 */

public interface HomeMVP {
    interface anglesView {
        void initializeAngleE20();

        void initializeAngleE70();

        void initializeAngleU30();

        void initializeAngleU90();
    }

    interface firstView {
        void initializeBarsCircle();

        void initializeBarsSquare();

        void initializeBarsHexagon();

        void initializeSheets();
    }

    interface iView {
        void initializeBeamsIPE();

        void initializeBeamsIPEAA();

        void initializeHEA();

        void initializeHEB();

        void initializeJIS();
    }

    interface tubeView {
        void initializeTubeSquare();

        void initializeTubeRectangular();
    }

    interface tubeRoundView {
        void initializeTubeRound();
    }

    interface upView {
        void initializeUPE();

        void initializeUPN();
    }

    interface uView {
        void initializeUChannel();
    }

    interface flatsView {
        void initializeFlatsView();
    }

    interface presenter {
        void initializeView(int position);
    }
}
